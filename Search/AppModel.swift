//
//  AppModel.swift
//  Search
//
//  Created by JustDoIt on 19.01.2018.
//  Copyright © 2018 DoIt. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Polyline

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

class AppModel: NSObject, MainViewControllerDelegate, CLLocationManagerDelegate {
    
    var navigationController: UINavigationController!
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    let mainViewController = MainViewController()
    
    override init() {
        super.init()
        GMSServices.provideAPIKey("AIzaSyDTWRoXpRlVyuzZT0NZh2Ih8exiYG4S-QQ")
        GMSPlacesClient.provideAPIKey("AIzaSyDTWRoXpRlVyuzZT0NZh2Ih8exiYG4S-QQ")
        
        self.locManager.requestAlwaysAuthorization()
        self.locManager.delegate = self
        self.locManager.startUpdatingLocation()
//        self.currentLocation = locManager.location

        if (self.currentLocation == nil){
            self.currentLocation = CLLocation.init(latitude: 46.458522, longitude: 30.751711)
        }

        self.navigationController = UINavigationController.init()
        mainViewController.delegate = self
        self.navigationController.setViewControllers([mainViewController], animated: true)
    }
    
    //MARK: MainViewControlDelegate
    func getLocation()->CLLocation {
        if(self.currentLocation != nil){
            return self.currentLocation
        }
        return CLLocation.init()
    }
    
    func sosButtonPressed() {
        if(!CLLocationCoordinate2DIsValid(self.currentLocation.coordinate) || !CLLocationCoordinate2DIsValid(self.mainViewController.selectedCoordinate)){
            return
        }
        self.setDirection(origin: "\(self.currentLocation.coordinate.latitude),\(self.currentLocation.coordinate.longitude)", direction: "\(self.mainViewController.selectedCoordinate.latitude),\(self.mainViewController.selectedCoordinate.longitude)")
    }
    
    func setDirection(origin: String, direction: String){
        GMDirectionpiApiCall.sharedInstance.getDirection(origin: origin, direction: direction) { json in
            let routes = json["routes"].arrayValue
            
            for route in routes{
                let routeOverViewPolyLine = route["overview_polyline"].dictionary
                let points = routeOverViewPolyLine?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                let polylines = GMSPolyline.init(path: path)
                
                polylines.strokeWidth = 4
                polylines.strokeColor = UIColor.blue
                polylines.map = self.mainViewController.mapView

                if var polylinePoints: [CLLocationCoordinate2D] = decodePolyline(points!){
                    for index in 0..<polylinePoints.count-1{
                        let marker = GMSMarker()
                        marker.position = polylinePoints[index]
                        marker.icon = #imageLiteral(resourceName: "marker")
                        marker.map = self.mainViewController.mapView

                        print(self.distanceBetween(firstPoint: polylinePoints[index], secondPoint: polylinePoints[index+1]))
                        print(self.coordinateFromDistance(firstPoint: polylinePoints[index], anglePoint: polylinePoints[index+1], distance: 50))
                    }

//                    var index = 0
//                    while(self.distanceBetween(firstPoint: polylinePoints[index], secondPoint: polylinePoints[index+1]) == 50.0){
//                        let middleCoordinate = self.middlePoint(firstPoint: polylinePoints[index], secondPoint: polylinePoints[index+1])
//
//                        //                            polylinePoints[index+1] = middleCoordinate
//                        polylinePoints.insert(middleCoordinate, at: index+1)
//                        print(polylinePoints.count)
//
//                        let marker = GMSMarker()
//                        marker.position = middleCoordinate
//                        marker.icon = #imageLiteral(resourceName: "marker")
//                        marker.map = self.mainViewController.mapView
//                        index = index + 1
//                    }
                }
            }
        }
    }

    func distanceBetween(firstPoint: CLLocationCoordinate2D, secondPoint: CLLocationCoordinate2D) -> Int{
        let R = 6371e3; // metres
        let φ1 = firstPoint.latitude.degreesToRadians
        let φ2 = secondPoint.latitude.degreesToRadians
        let Δφ = (secondPoint.latitude-firstPoint.latitude).degreesToRadians
        let Δλ = (secondPoint.longitude-firstPoint.longitude).degreesToRadians

        let a = sin(Δφ/2) * sin(Δφ/2) + cos(φ1) * cos(φ2) * sin(Δλ/2) * sin(Δλ/2);
        let c = 2 * atan2(sqrt(a), sqrt(1-a));

        let d = R * c;
        let distance: Int = Int.init(d)
        return distance;
    }

    func coordinateFromDistance(firstPoint: CLLocationCoordinate2D, anglePoint: CLLocationCoordinate2D, distance: Double) -> CLLocationCoordinate2D{
        let R = 6371e3; // metres

        //getBearing
        let φ11 = firstPoint.latitude.degreesToRadians
        let φ21 = anglePoint.latitude.degreesToRadians
        let Δφ1 = (anglePoint.latitude-firstPoint.latitude).degreesToRadians
        let Δλ1 = (anglePoint.longitude-firstPoint.longitude).degreesToRadians

        let a = sin(Δφ1/2) * sin(Δφ1/2) + cos(φ11) * cos(φ21) * sin(Δλ1/2) * sin(Δλ1/2);
        let c = 2 * atan2(sqrt(a), sqrt(1-a));


        let φ1 = firstPoint.latitude.degreesToRadians
        let λ1 = firstPoint.longitude.degreesToRadians

        let φ2 = asin( sin(φ1)*cos(distance/R) + cos(φ1)*sin(distance/R)*cos(c) );
        let λ2 = λ1 + atan2(sin(c)*sin(distance/R)*cos(φ1), cos(distance/R)-sin(φ1)*sin(φ2));

        return CLLocationCoordinate2D.init(latitude: φ2.radiansToDegrees, longitude: λ2.radiansToDegrees)
    }

    func middlePoint(firstPoint: CLLocationCoordinate2D, secondPoint: CLLocationCoordinate2D) -> CLLocationCoordinate2D{
        var x = 0.0
        var y = 0.0
        var z = 0.0

        let latitude = firstPoint.latitude * Double.pi / 180
        let longitude = firstPoint.longitude * Double.pi / 180

        x += cos(latitude) * cos(longitude)
        y += cos(latitude) * sin(longitude)
        z += sin(latitude)

        x = x / 2
        y = y / 2
        z = z / 2

        let centralSquareRoot = sqrt(x * x + y * y)
        let centralLongitude = atan2(y, x)
        let centralLatitude = atan2(z, centralSquareRoot)

        return CLLocationCoordinate2D.init(latitude: centralLatitude.radiansToDegrees, longitude: centralLongitude.radiansToDegrees)
    }
}
