//
//  GMApiCall.swift
//  Search
//
//  Created by JustDoIt on 19.01.2018.
//  Copyright © 2018 DoIt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class GMDirectionpiApiCall: NSObject {
    
    static let sharedInstance = GMDirectionpiApiCall()
    
    private override init(){}
    
    func getDirection(origin: String, direction: String, completion: @escaping (JSON)->Void){        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(direction)&key=\(GMApiInfo.apiKey)"
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            switch response.result{
            case .success(let data):
                let jsonData = JSON(data)
                completion(jsonData)
            case.failure(let error):
                print(error)   
            }
        })
    }
}

