//
//  GMApiInfo.swift
//  Search
//
//  Created by JustDoIt on 19.01.2018.
//  Copyright © 2018 DoIt. All rights reserved.
//

import UIKit

class GMApiInfo: NSObject {

    static var apiKey:String {
        get{
            if let key = UserDefaults.standard.value(forKey: "GoogleMapsApiKey"){
                return key as! String
            }
            return ""
        }
        set{
            if(!newValue.isEmpty){
                UserDefaults.standard.set(newValue, forKey: "GoogleMapsApiKey")
            }
        }
    }
    
}
