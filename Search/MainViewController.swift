//
//  ViewController.swift
//  Search
//
//  Created by JustDoIt on 19.01.2018.
//  Copyright © 2018 DoIt. All rights reserved.
//

import UIKit
import GoogleMaps

protocol MainViewControllerDelegate {
    func getLocation()->CLLocation
    func sosButtonPressed()
}

class MainViewController: UIViewController, GMSMapViewDelegate {

    public var selectedCoordinate: CLLocationCoordinate2D!

    var mapView: GMSMapView!
    var delegate: MainViewControllerDelegate!
    
    override func loadView() {
        super.loadView()
        let currentLocation = delegate.getLocation()
        let camera = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 10.0)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.mapView.isMyLocationEnabled = true
        self.mapView.delegate = self
        self.view = mapView
        
        let sosButton = UIButton.init(type: .custom)
        sosButton.backgroundColor = UIColor.orange
        sosButton.setTitle("Провести маршрут", for: .normal)
        sosButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        sosButton.translatesAutoresizingMaskIntoConstraints = false
        sosButton.addTarget(self, action: #selector(sosButtonPressed), for: .touchUpInside)
        self.view.addSubview(sosButton)
        
        let sosButtonBottomConstraint = NSLayoutConstraint.init(item: sosButton, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0)
        let sosButtonCenterXConstraint = NSLayoutConstraint.init(item: sosButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let sosButtonWidthConstraint = NSLayoutConstraint.init(item: sosButton, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 1.0, constant: 0)
        let sosButtonHeightConstraint = NSLayoutConstraint.init(item: sosButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        NSLayoutConstraint.activate([sosButtonBottomConstraint, sosButtonCenterXConstraint, sosButtonWidthConstraint, sosButtonHeightConstraint])
    }


    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {

//        if(self.selectedCoordinate.longitude == coordinate.longitude && self.selectedCoordinate.latitude == coordinate.latitude){
            let marker = GMSMarker()
            marker.position = coordinate
            marker.icon = #imageLiteral(resourceName: "marker")
            marker.map = self.mapView
            selectedCoordinate = coordinate
//        }
//        else {
//            let alertcontroller = UIAlertController.init()
//            let okButton = UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.cancel) { (alertAction) in
//                self.view.endEditing(true)
//            }
//            alertcontroller.addAction(okButton)
//            self.present(alertcontroller, animated: true, completion: nil)
//        }
    }

    @objc func sosButtonPressed(){
        delegate.sosButtonPressed()
    }
}

